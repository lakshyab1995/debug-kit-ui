package com.lakshya.debug.debugkitui.debugkit;

import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by Lakshya on 3/24/2018.
 */


/**
 * Debug Kit UI is used to print logs
 * on the UI window
 *
 */
public class DebugWindow {

    private static final String TAG = DebugWindow.class.getSimpleName();
    public static boolean mIsDebugEnabled=false;
    private static DebugWindowView mDebugWindowView;

    public DebugWindow(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            Log.d(TAG, "Lifecycle callbacks registered");
            ((Application) activity.getApplicationContext()).registerActivityLifecycleCallbacks(activityLifecycleCallbacks);
            if (mIsDebugEnabled) {
                if (mDebugWindowView == null) {
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    mDebugWindowView = new DebugWindowView(activity.getApplicationContext());
                    activity.addContentView(mDebugWindowView, params);
                    mDebugWindowView.bringToFront();
                }
            }
        }
    }

    private Application.ActivityLifecycleCallbacks activityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {
        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {
            Log.d(TAG, "onActivityResumed");
            if (mIsDebugEnabled) {
                if (mDebugWindowView != null) {
                    removeFromParent(mDebugWindowView);
                    FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    activity.addContentView(mDebugWindowView, params);
                    mDebugWindowView.bringToFront();
                }
            }
        }

        @Override
        public void onActivityPaused(Activity activity) {
            Log.d(TAG, "onActivityPaused");
            if (mIsDebugEnabled) {
                if (mDebugWindowView != null) {
                    if (mDebugWindowView.getParent() != null) {
                        ((ViewGroup) mDebugWindowView.getParent()).removeView(mDebugWindowView);
                    }
                }
            }
        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {

        }
    };

    public static void log(String log) {
        if (mDebugWindowView != null) {
            if (!TextUtils.isEmpty(log)) {
                mDebugWindowView.log(log);
            }
        }
    }

    private static void removeFromParent(View view) {
        if (view == null) {
            Log.d(TAG, "[ERROR] Trying to remove parent for a null view object");
            return;
        }
        ViewGroup parent = (ViewGroup) view.getParent();
        if (parent != null) {
            parent.removeView(view);
        }
    }
}
