package com.lakshya.debug.debugkitui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.lakshya.debug.debugkitui.debugkit.DebugWindow;
import com.lakshya.debug.debugkitui.debugkit.DebugWindowView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        DebugWindow.mIsDebugEnabled=true;
        DebugWindow debugWindow=new DebugWindow(this);
        DebugWindow.log("First Log");
    }
}
