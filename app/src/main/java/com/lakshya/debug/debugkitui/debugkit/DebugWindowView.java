package com.lakshya.debug.debugkitui.debugkit;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.lakshya.debug.debugkitui.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lakshya on 3/24/2018.
 */

public class DebugWindowView extends FrameLayout{

    private static final String TAG = DebugWindowView.class.getSimpleName();
    private ListView mDebugList;
    private View mDebugStatus;
    private Button mOpenBtn;
    private List<String> mDebugLogsList;
    private ArrayAdapter mDebugAdapter;
    private RelativeLayout mClearSection;
    private boolean mFlag = true;
    private float mDX;
    private float mDY;
    private int mLastAction;
    private Context mContext;
    private Handler mHandler;
    LinearLayout layoutParent;

    public DebugWindowView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    private void init() {

        View debugWindow = LayoutInflater.from(mContext).inflate(R.layout.debug_window_layout, null);
        mHandler=new Handler(Looper.getMainLooper());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.gravity = Gravity.TOP;
        //addView(debugWindow, layoutParams);
        layoutParent = (LinearLayout) debugWindow.findViewById(R.id.layoutParent);
        mClearSection = (RelativeLayout) debugWindow.findViewById(R.id.clearSection);
        RelativeLayout debugRoot = (RelativeLayout) debugWindow.findViewById(R.id.root);
        mOpenBtn = (Button) debugWindow.findViewById(R.id.openBtn);
        mDebugList = (ListView) debugWindow.findViewById(R.id.debugList);
        Button clearBtn = (Button) debugWindow.findViewById(R.id.clearBtn);
        TextView debugKitVer= (TextView) debugWindow.findViewById(R.id.debug_kitVer);
        debugKitVer.setText("V1.0");
        mDebugStatus = debugWindow.findViewById(R.id.debugStatus);
        layoutParent.removeAllViews();
        layoutParent.addView(debugRoot, 0);
        layoutParent.addView(mDebugList, 1);
        layoutParent.addView(mDebugStatus, 1);
        layoutParent.addView(mClearSection, -1);
        addView(layoutParent,layoutParams);
        mDebugLogsList = new ArrayList<>();
        mDebugAdapter = new ArrayAdapter<>(mContext, R.layout.item_debug_window, android.R.id.text1, mDebugLogsList);
        mDebugList.setAdapter(mDebugAdapter);
        layoutParent.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent event) {


                float newX, newY;
                WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
                Display display = windowManager.getDefaultDisplay();
                Point point = new Point();
                display.getSize(point);
                int screenHeight = point.y;
                int screenWidth = point.x;

                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:

                        mDX = view.getX() - event.getRawX();
                        mDY = view.getY() - event.getRawY();
                        mLastAction = MotionEvent.ACTION_DOWN;
                        break;

                    case MotionEvent.ACTION_MOVE:

                        newX = event.getRawX() + mDX;
                        newY = event.getRawY() + mDY;

                        // check if the view out of screen
                        if ((newX <= 0 || newX >= screenWidth - view.getWidth()) || (newY <= 0 || newY >= screenHeight - view.getHeight())) {
                            mLastAction = MotionEvent.ACTION_MOVE;
                            break;
                        }

                        view.setX(newX);
                        view.setY(newY);
                        view.bringToFront();

                        mLastAction = MotionEvent.ACTION_MOVE;

                        break;

                    case MotionEvent.ACTION_UP:
                        if (mLastAction == MotionEvent.ACTION_DOWN)
                            break;

                    default:
                        return false;
                }
                return true;
            }
        });

        mOpenBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mFlag) {
                    mDebugList.setVisibility(View.VISIBLE);
                    mClearSection.setVisibility(View.VISIBLE);
                    mOpenBtn.setText(getResources().getString(R.string.collapse_txt));
                    mFlag = false;
                } else {
                    mDebugList.setVisibility(View.GONE);
                    mClearSection.setVisibility(View.GONE);
                    mOpenBtn.setText(getResources().getString(R.string.expand_txt));
                    mFlag = true;
                }

            }
        });
        clearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDebugLogsList.clear();
                mDebugAdapter.notifyDataSetChanged();
            }
        });
    }

    public void log(final String log) {
        if (!TextUtils.isEmpty(log)) {
            Log.d(TAG, log);
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mDebugLogsList.add(log);
                    mDebugAdapter.notifyDataSetChanged();
                    mDebugList.setSelection(mDebugAdapter.getCount() - 1);
                    //set colors on mDebugStatus on Debug Kit UI, check for status by filtering results from log string
                    mDebugStatus.setBackgroundColor(getResources().getColor(R.color.debug_green));
                }
            });
        }
    }

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        int orientation=newConfig.orientation;
        switch (orientation){
            case Configuration.ORIENTATION_PORTRAIT:
                if(layoutParent !=null){
                    layoutParent.setX(0);
                    layoutParent.setY(0);
                }
                break;
            case Configuration.ORIENTATION_LANDSCAPE:
                if(layoutParent != null){
                    layoutParent.setX(0);
                    layoutParent.setY(0);
                }
                break;
        }
    }
}
